package constructive;

/**
 *
 * @author elkrari
 */


import tsp.Instance;

public class SmallestInsertion implements Constructive {

    /**
     * Build a new tour following the smallest insertion
     * strategy with a random starting node/city
     * @param tsp An instance of the problem
     * @return A new tour
     */
    @Override
    public int[] newTour(Instance tsp) {
        int length = tsp.getDimension();
        int tour[] = new int[length];
        for (int k = 0; k < length; k++) {
            tour[k] = k + 1;
        }
        int i = 0;
        int Vi, n, temp, dist;
        Vi = (int) (Math.random() * length);
        temp = tour[0];
        tour[0] = tour[Vi];
        tour[Vi] = temp;
        Vi = (int) (Math.random() * length - 1) + 1;
        temp = tour[1];
        tour[1] = tour[Vi];
        tour[Vi] = temp;
        for (i = 2; i < length; i++) {
            Vi = (int) (Math.random() * length - i) + i;
            temp = tour[i];
            tour[i] = tour[Vi];
            tour[Vi] = temp;
            n = 0;
            dist = tsp.distance(tour[0],tour[Vi]) + tsp.distance(tour[1],tour[Vi]);
            for (int j = 1; j < i; j++)
                if (dist > tsp.distance(tour[j],tour[Vi]) + tsp.distance(tour[j + 1],tour[Vi]))
                    n = j;
            tour = shift(tour, n, i);
        }
        return tour;
    }


    private int[] shift(int[] tour, int start, int end){
        int temp = tour[end];
        for(int i=end;i>start;i--){
            tour[i]=tour[i-1];
        }
        tour[start]=temp;
        return tour;
    }
}
