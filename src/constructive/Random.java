package constructive;

/**
 *
 * @author elkrari
 */


import tsp.Instance;

/**
 *
 * @author elkrari
 */
public class Random implements Constructive{

    /**
     * Build a new tour randomly
     * with a random starting node/city
     * @param tsp An instance of the problem
     * @return A new tour
     */
    @Override
    public int[] newTour(Instance tsp) {
        java.util.Random rand = new java.util.Random();
        int r = rand.nextInt(tsp.getDimension());

        return newTour(tsp,r+1);
    }

    /**
     * Build a new tour randomly
     * with a predefined starting city
     * @param tsp An instance of the problem
     * @return A new tour
     */
    public int[] newTour(Instance tsp, int start) {
        int length = tsp.getDimension();
        int[] S = new int[length];

        for(int i=1;i<=length;i++)
            S[i-1]=i;



        int r,s;

        s = S[0];
        S[0] = S[start-1];
        S[start-1] = s;

        for(int i=1;i<length;i++){
            java.util.Random rand = new java.util.Random();
            r = rand.nextInt((length-1 - i) + 1) + i;
            s = S[i];
            S[i] = S[r];
            S[r] = s;
        }

        return S;
    }

}
