package generator;

/**
 *
 * @author elkrari
 */


import tsp.City;
import tsp.Instance;

import java.util.ArrayList;

import static tsp.City.*;

public class InstanceGenerator {

    /**
     * Generate a new TSP instance with randomly
     * distributed nodes over the geographic space
     * delimited by {@code width} and {@code height}
     * @param dimension Instance size (number of cities)
     * @param edgeWeightType Edge Weight Type: Euclidean or Geographic
     * @param width The width of geographic space
     * @param height The height of geographic space
     * @return A new instance randomly generated
     */
    public Instance generate(int dimension, String edgeWeightType, int width, int height){
        Instance tsp = new Instance();

        tsp.setDimension(dimension);
        tsp.setEdgeWeightType(edgeWeightType);

        //-----Generate TSP Coordinates-----
        ArrayList<City> coordinates = new ArrayList<>();
        City cc;
        for(int i=0;i<dimension;i++){
            do{
                cc = new City((float)Math.random() * width,(float)Math.random() * height);
            }while(i!=0 && exist(coordinates,cc,i-1));
            coordinates.add(cc);
        }
        tsp.setCities(coordinates);

        int[][] distances = new int[dimension][dimension];
        if("EUC_2D".equals(edgeWeightType))
            for (int i = 0; i < dimension - 1; i++) {
                for (int j = i + 1; j < dimension; j++) {
                    distances[i][j] = distances[j][i] = distEuc(coordinates.get(i), coordinates.get(j));
                }
            }
        else if(edgeWeightType.equals("GEO"))
            for (int i = 0; i < dimension - 1; i++) {
                for (int j = i + 1; j < dimension; j++) {
                    distances[i][j] = distances[j][i] = distGeo(coordinates.get(i), coordinates.get(j));
                }
            }

        tsp.setDistances(distances);

        return tsp;
    }

    private boolean exist(ArrayList<City> setCoordinates, City coordinates, int limit){
        for(int i=0;i<=limit;i++){
            if(setCoordinates.get(i).equals(coordinates))
                return true;
        }
        return false;
    }
}
