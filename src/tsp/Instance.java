package tsp;

import generator.PermutationGenerator;

import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.*;
import java.util.function.BiFunction;

import static tsp.City.*;


/**
 *
 * @author elkrari
 */
public class Instance {

    private int dimension;
    private String edgeWeightType;
    private ArrayList<City> cities;
    private int[][] distances;
    private int[] nForb;

    private static BiFunction dist;

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public void setEdgeWeightType(String edgeWeightType) {
        this.edgeWeightType = edgeWeightType;
    }

    public void setCities(ArrayList<City> cities) {
        this.cities = cities;
    }

    public void setDistances(int[][] distances) {
        this.distances = distances;
    }

    public int getDimension() {
        return dimension;
    }

    public String getEdgeWeightType() {
        return edgeWeightType;
    }

    public ArrayList<City> getCities() {
        return cities;
    }

    public int[][] getDistances() {
        return distances;
    }



    public Instance(){

    }

    public Instance(File instance) {
        String line;

        try {
            FileInputStream fStream = new FileInputStream(instance);
            DataInputStream in = new DataInputStream(fStream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            /*----- DIMENSION -----*/
            do {
                line = br.readLine();
            } while (!line.contains("DIMENSION"));
            StringTokenizer st = new StringTokenizer(line, ":");
            st.nextToken();
            line = st.nextToken().substring(1);
            dimension = Integer.parseInt(line);

            /*----- EDGE WEIGHT TYPE -----*/
            do {
                line = br.readLine();
            } while (!line.contains("EDGE_WEIGHT_TYPE"));
            st = new StringTokenizer(line, ":");
            st.nextToken();
            edgeWeightType = st.nextToken().substring(1);

            /*----- NODE COORD SECTION -----*/
            cities = new ArrayList<>();
            while (!br.readLine().contains("NODE_COORD_SECTION")) {}
            cities.add(new City());
            for (int i = 0; i < dimension; i++) {
                cities.add(lineToCity(br));
            }

            /*----- DISTANCES -----*/
            City c = new City();
            if(dimension<5000) {
                distances = new int[dimension + 1][dimension + 1];
                if ("EUC_2D".equals(edgeWeightType))
                    for (int i = 1; i < dimension; i++) {
                        for (int j = i + 1; j <= dimension; j++) {
                            distances[i][j] = distances[j][i] = c.distEuc(cities.get(i), cities.get(j));
                        }
                    }
                else if (edgeWeightType.equals("GEO"))
                    for (int i = 1; i < dimension; i++) {
                        for (int j = i + 1; j <= dimension; j++) {
                            distances[i][j] = distances[j][i] = c.distGeo(cities.get(i), cities.get(j));
                        }
                    }
            }
            //if ("EUC_2D".equals(edgeWeightType))
              //  dist = new City()::distEuc;

            in.close();
        } catch (Exception e) {
            System.err.println("Error: " + e.getMessage());
        }
    }


    public City lineToCity(BufferedReader br) throws IOException {
        StringTokenizer line = new StringTokenizer(br.readLine(), " ");
        City V = new City();
        line.nextToken();
        V.x = Float.parseFloat(line.nextToken());
        V.y = Float.parseFloat(line.nextToken());
        return V;
    }

    public int distance(int i, int j){
        if(dimension<5000)
            return distances[i][j];
        else
            if("EUC_2D".equals(edgeWeightType))
                return new City().distEuc(cities.get(i), cities.get(j));
            else
                return new City().distGeo(cities.get(i), cities.get(j));
    }

    @Override
    public String toString() {
        return "Instance{" +
                "dimension=" + dimension +
                ", distances=" + toStringMatrix(distances) +
                '}';
    }

    private String toStringMatrix(int[][] mat){
        String M="";
        M = M.concat("\n");
        for(int i=1;i<=dimension;i++) {
            for (int j = 1; j <= dimension; j++) {
                M = M.concat(Integer.toString(mat[i][j])+"\t");
            }
            M = M.concat("\n");
        }

        /*for (int[] row: mat){
            M = M.concat(Arrays.toString(row)+"\n");
        }*/

        return M;
    }

}