package tsp;

import constructive.Random;
import utils.Similarity;

import java.util.Arrays;
import java.util.Objects;

public class Solution {
    private int[] tour;
    /*
    Position array size is n+1
    Nodes are indexed from 1 to n
    Position values are from 0 to n-1
     */
    private int[] pos;
    private int fitness;

    /**
     * Construct a new Solution from the
     * given instance and tour
     * @param tsp A TSP instance
     * @param tour A permutation of cities
     */
    public Solution(Instance tsp, int[] tour) {
        this.tour = tour;

        this.pos = new int[tsp.getDimension()+1];
        for(int x=0;x<tsp.getDimension();x++){
            pos[tour[x]]=x;
        }

        this.setFitness(tour,tsp);

        //this.setHash(new Hash5(this, tsp).getHash());


    }

    /**
     * Construct a new Solution from the
     * given instance with a random tour
     * @param tsp A TSP instance
     */
    public Solution(Instance tsp) {
        this.tour = new Random().newTour(tsp);

        this.pos = new int[tsp.getDimension()+1];
        for(int x=0;x<tsp.getDimension();x++){
            pos[tour[x]]=x;
        }

        this.setFitness(tour,tsp);

        //this.setHash(new Hash5(this, tsp).getHash());

    }

    public Solution(){

    }

    public void setFitness(int[] tour, Instance tsp) {
        int f = 0;
        for (int x = 0; x < tsp.getDimension(); x++) {
            f += tsp.distance(tour[x],tour[(x + 1) % tsp.getDimension()]);//[tour[x]][tour[(x + 1) % tsp.getDimension()]];
        }
        this.fitness = f;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }

    public int[] getTour() {
        return tour;
    }

    public int[] getPos() {
        return pos;
    }

    public int getFitness() {
        return fitness;
    }

    @Override
    public String toString() {
        return "Solution{" +
                "tour=" + Arrays.toString(tour) +
                ", fitness=" + fitness +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Solution solution = (Solution) o;
        return new Similarity().areSimilar(this,solution);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tour, pos, fitness);
    }

    public Solution copy(){
        Solution S = new Solution();
        S.tour = Arrays.copyOf(tour,tour.length);
        S.pos = Arrays.copyOf(pos,pos.length);
        S.fitness = fitness;

        return S;
    }
}
