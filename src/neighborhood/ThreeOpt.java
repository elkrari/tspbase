package neighborhood;

import tsp.Instance;
import tsp.Solution;
import utils.Movement;

//TODO
public class ThreeOpt implements Neighborhood{
    @Override
    public Solution move(Instance tsp, Solution s, Movement mvt) {
        int i=mvt.getNodes()[0],j=mvt.getNodes()[1],k=mvt.getNodes()[2];
        int x,n= s.getTour().length,z=0;
        int[] Sn = new int[n];
        if (mvt.getOption() == 1) {
            for (x = i + 1; x <= j; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = k + 1; x < n; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = 0; x <= i; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = j + 1; x <= k; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
        } else if (mvt.getOption() == 2) {
            for (x = i + 1; x <= j; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = k + 1; x < n; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = 0; x <= i; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = k; x >= j + 1; x--) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
        } else if (mvt.getOption() == 3) {
            for (x = i + 1; x <= j; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = i; x >= 0; x--) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = n - 1; x >= k + 1; x--) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = j + 1; x <= k; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
        } else {
            for (x = i + 1; x <= j; x++) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = k; x >= j + 1; x--) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = i; x >= 0; x--) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
            for (x = n - 1; x >= k + 1; x--) {
                s.getPos()[s.getTour()[x]]=z;
                Sn[z++] = (s.getTour()[x]);
            }
        }
        /*UNCOMPLETE*/
        return s;
    }

    @Override
    public Integer fitnessDelta(Instance I, Solution s, Movement mvt) {
        return null;
    }

}
