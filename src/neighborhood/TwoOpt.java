package neighborhood;

import tsp.*;
import utils.Movement;


public class TwoOpt implements Neighborhood {


    @Override
     public Solution move(Instance tsp, Solution s, Movement mvt) {
        if (mvt.getNodes().length<2)
            mvt = randomMVT(s.getTour().length);
        int i=mvt.getNodes()[0]+1, j=mvt.getNodes()[1];
        if(j<i){
            int k = j;
            j=i;
            i=k;
        }
        Swap sw = new Swap();
        while (i < j) {
            s = sw.move(tsp, s, new Movement(i,j));
            i++;
            j--;
        }


        return s;
    }

    @Override
    public Integer fitnessDelta(Instance tsp, Solution s, Movement mvt) {
        int i=mvt.getNodes()[0], j=mvt.getNodes()[1];

        int n= tsp.getDimension();
        final int i1 = ((i + 1) % n + n) % n;
        final int j1 = ((j + 1) % n + n) % n;
        return tsp.distance(s.getTour()[i],s.getTour()[j])
                +tsp.distance(s.getTour()[i1],s.getTour()[j1])
                -tsp.distance(s.getTour()[i],s.getTour()[i1])
                -tsp.distance(s.getTour()[j],s.getTour()[j1]);
    }
    
    private Movement randomMVT(int n){
        int i,j;
        i = (int) (Math.random()*n-1);
        j=i;
        while(j>i-2 && j<i+2)
            j = (int) (Math.random()*n);
        
        return new Movement(i,j);
    }

}
