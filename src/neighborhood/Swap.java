package neighborhood;

import tsp.Instance;
import tsp.Solution;
import utils.Movement;

public class Swap implements Neighborhood {
    @Override
    public Solution move(Instance tsp, Solution s, Movement mvt) {
        int i = mvt.getNodes()[0], j = mvt.getNodes()[1];
        s.setFitness(s.getFitness()+fitnessDelta(tsp, s, mvt));
        int k = s.getTour()[i];

        int x = s.getPos()[k];
        s.getPos()[s.getTour()[i]] = s.getPos()[s.getTour()[j]];
        s.getPos()[s.getTour()[j]] = x;

        s.getTour()[i] = s.getTour()[j];
        s.getTour()[j] = k;

        return s;
    }

    @Override
    public Integer fitnessDelta(Instance I, Solution s, Movement mvt) {
        int n = I.getDimension(), i = mvt.getNodes()[0], j = mvt.getNodes()[1];
        if(i==0 && j==(n-1)){
            return  I.getDistances()[s.getTour()[0]][s.getTour()[n-2]]
                    +I.getDistances()[s.getTour()[n-1]][s.getTour()[1]]
                    -I.getDistances()[s.getTour()[0]][s.getTour()[1]]
                    -I.getDistances()[s.getTour()[n-1]][s.getTour()[n-2]];
        }
        if((j%n+n)%n==((i+1)%n+n)%n || (j%n+n)%n==((i-1)%n+n)%n){
            return I.getDistances()[s.getTour()[(((i-1)%n+n)%n)]][s.getTour()[j]]
                    +I.getDistances()[s.getTour()[i]][s.getTour()[(((j+1)%n+n)%n)]]
                    -I.getDistances()[s.getTour()[(((i-1)%n+n)%n)]][s.getTour()[i]]
                    -I.getDistances()[s.getTour()[j]][s.getTour()[(((j+1)%n+n)%n)]];
        }
        return I.getDistances()[s.getTour()[(((i-1)%n+n)%n)]][s.getTour()[j]]
                +I.getDistances()[s.getTour()[j]][s.getTour()[i+1]]
                +I.getDistances()[s.getTour()[j-1]][s.getTour()[i]]
                +I.getDistances()[s.getTour()[i]][s.getTour()[(((j+1)%n+n)%n)]]
                -I.getDistances()[s.getTour()[(((i-1)%n+n)%n)]][s.getTour()[i]]
                -I.getDistances()[s.getTour()[i]][s.getTour()[i+1]]
                -I.getDistances()[s.getTour()[j-1]][s.getTour()[j]]
                -I.getDistances()[s.getTour()[j]][s.getTour()[(((j+1)%n+n)%n)]];
    }
}
