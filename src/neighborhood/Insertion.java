package neighborhood;

import tsp.Instance;
import tsp.Solution;
import utils.Movement;


public class Insertion implements Neighborhood {
    @Override
    public Solution move(Instance tsp, Solution s, Movement mvt) {
        int i = mvt.getNodes()[0], j = mvt.getNodes()[1];
        int x = s.getTour()[i];// y = S.getPos()[j];
        s.setFitness(s.getFitness()+fitnessDelta(tsp, s, mvt));

        if(i<j) {
            for (int k = i; k < j; k++) {
                s.getTour()[k] = s.getTour()[k + 1];
                s.getPos()[s.getTour()[k]]--;
            }
            s.getTour()[j] = x;
            s.getPos()[x] = j;
        }else{
            for (int k = i; k > j; k--) {
                s.getTour()[k] = s.getTour()[k - 1];
                s.getPos()[s.getTour()[k]]++;
            }
            s.getTour()[j] = x;
            s.getPos()[x] = j;
        }
        return s;
    }

    @Override
    public Integer fitnessDelta(Instance I, Solution s, Movement mvt) {
        int i = mvt.getNodes()[0], j = mvt.getNodes()[1];
        int n = I.getDimension();
        int[] t = s.getTour();
    
        final int i1 = ((i + 1) % n + n) % n;
        final int j1 = ((j + 1) % n + n) % n;
        final int i_1 = ((i - 1) % n + n) % n;
        return I.distance(t[i_1],t[i1])
                +I.distance(t[i],t[j])
                +I.distance(t[i],t[j1])
                -I.distance(t[i_1],t[i])
                -I.distance(t[i],t[i1])
                -I.distance(t[j],t[j1]);
    }
}
