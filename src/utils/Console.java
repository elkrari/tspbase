package utils;

public class Console {

    public static void flushPrint(String head, String str){
        clearScreen();
        System.out.println(head);
        System.out.println("-----"+str+"-----");
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public final static void clearConsole()
    {
        try
        {
            final String operatingSystem = System.getProperty("os.name");

            if (operatingSystem .contains("Windows")) {
                java.lang.Runtime.getRuntime().exec("cls");
            }
            else {
                java.lang.Runtime.getRuntime().exec("clear");
            }
        }
        catch (final Exception e)
        {
            //  Handle any exceptions.
        }
    }
}
