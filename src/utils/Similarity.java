/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package utils;

import tsp.Solution;

/**
 *
 * @author elkrari
 */
public class Similarity {
    
    /*public boolean areSimilar(Solution S1, Solution S2){
        int[] t1 = S1.getTour(), t2 = S2.getTour();

        if(t1.length != t2.length || S1.getFitness() != S2.getFitness())
            return false;
        
        int shift = 0;
        while(t1[0] != t2[shift])
            shift++;
        
        int i=0, j=shift, count=0, n=t1.length;
        while(count<n && t1[i] == t2[(((j)%n+n)%n)]){
            i++; j++; count++;
        }
        return count==n;
        
    }*/

    public boolean areSimilar(Solution S1, Solution S2){
        int l = S1.getTour().length;

        int[] t1 = S1.getTour(), t2 = S2.getTour(), p2 = S2.getPos();

        //return Math.abs(S1.getHash()-S2.getHash())<0.000001;

        for(int i=0;i<l;i++)
            if ((!new Edge(t1[i], t1[(i + 1) % l]).equals(new Edge(t2[p2[t1[i]]], t2[((p2[t1[i]] + 1) % l + l) % l])))
                    && (!new Edge(t1[i], t1[(i + 1) % l]).equals(new Edge(t2[p2[t1[i]]], t2[((p2[t1[i]] - 1) % l + l) % l]))))
                return false;

        return  true;
    }

    public double simRate(Solution S1, Solution S2){
        double k = 0;
        int l = S1.getTour().length;

        int[] t1 = S1.getTour(), t2 = S2.getTour(), p2 = S2.getPos();

        for(int i=0;i<l;i++) {
            if ((new Edge(t1[i], t1[(i + 1) % l]).equals(new Edge(t2[p2[t1[i]]], t2[((p2[t1[i]] + 1) % l + l) % l])))
                    || (new Edge(t1[i], t1[(i + 1) % l]).equals(new Edge(t2[p2[t1[i]]], t2[((p2[t1[i]] - 1) % l + l) % l])))) {
                k++;
//                System.out.println("t1[i], t1[i + 1] = ["+t1[i]+","+t1[(i + 1) % l]+
//                        "] / t2[p2[t1[i]]], t2[p2[t1[i]] + 1] = ["+t2[p2[t1[i]]]+","+t2[((p2[t1[i]] + 1) % l + l) % l]+
//                        "] / t2[p2[t1[i]]], t2[p2[t1[i]] - 1] = ["+t2[p2[t1[i]]]+","+t2[((p2[t1[i]] + -1) % l + l) % l]+"]");
            }

        }

        System.out.println("k="+k);
        return  k/l;
    }

    private class Edge{
        int x;
        int y;

        public Edge(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Edge edge = (Edge) o;

            if ((x == edge.x && y == edge.y) || (x == edge.y && y == edge.x))
                return true;

            return false;
        }

        /*@Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }*/
    }
}
